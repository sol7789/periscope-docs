---
sidebar: auto
---

# Getting Started

## Download the installer

Download an install the latest release from the
[Gitlab Release Page](https://gitlab.com/periscope-authenticator/periscope/-/releases)

### Windows

Download the `setup.exe` and run it. This will install Periscope for your user.

### macOS

For new Apple Silicon Macs (M1, M2...) download the `darwin-arm64.zip`

For Intel Macs download the `darwin-x64.zip`

Double click to unzip the application and drag it to the `Applications` folder

### Ubuntu/Debian

Download the `.deb` and install using `dpkg`

```
dpkg -i /path/to/periscope.deb
```

### Fedora/CentOS/RHEL/Rocky

Download the `.rpm` and install using `rpm`
```
rpm -i /path/to/periscope.rpm
```


## Server Setup

Once the application loads you will be greeted by the Server Settings pane.

![server-setting](../assets/server-settings.png)

Add in the servers that you would like to proxy authenticate for.

On this page all that's required is the Name, Server Address and Local Port.

For the Server Address all that's required is the hostname and port if different from standard
(e.g. for https://example.com/example -> https://example.com). Any other parts of the URL will be ignored


::: warning
The Local Port must be a free port on your local computer and greater than 1024.
:::

## General Settings

If the servers you are attempting to contact is behind a corporate proxy would need to set
the proxy in the general settings shown below

![general-settings](../assets/general-settings.png)

You will also need to enable `Use Proxy` in the Server Settings for all servers that
require it.

## Adding Conditions

Periscope needs to know when to open a browser window to authenticate.

The conditions need to be set to elements of the HTTP Response which indicate that an authentiation is needed.

Example: If your authentication involves an HTTP Redirect your Condition Set may look like below

![condition-sets](../assets/condition-sets.png)

::: tip
This section may need tweaking if it starts to catch too many requests or the pop-up is not appearing.
:::

Periscope also supports having multiple Condition Sets if different servers require different conditions.

By default all Servers will use the default set but it can be changed in the Server Settings

# Using Periscope for Commands

When using a service through Periscope you need to point the application at Periscope instead
of the real service.

Example:

If you want to clone `https://gitlab.com/periscope-authenticator/periscope`

You will need to setup `https://gitlab.com` as a Server with a port (e.g. 10001) and the proper Condition Set

Then to clone you need to change the host to localhost with the port and http

```
git clone http://localhost:10001/periscope-authenticator/periscope.git
```
