---
sidebar: auto
---

# Development Setup

This project is based on Electron v25 and uses React for the UI.

For development we are using Node v18 which is latest LTS at time of writing.

For our package manager we are using `yarn` so before starting make sure it is installed.
It may come preinstalled with some installs of node.

```
npm install -g yarn
```

Once it is installed navigate to the repository and execute the following to start the application
in development mode.

```
yarn install
yarn start
```

If you are behind a Corporate Proxy you may have to set `ELECTRON_GET_USE_HTTP=1` and `NODE_EXTRA_CA_CERTS=<PATH TO CERTIFICATE>`
if you get errors about pulling electron or self-signed certificate in certificate chain.

## Helpful Extensions

If you are using VSCode to develop the following extensions are recommended to help with linting, typing and conforming to formatting.

- ESLint
- Prettier - Code formatter

Some additional general extensions that help in the development process

- Code Spell Checker
- Gitlens
- TODO Highlight

# Developing

## Directory Structure

```
├── src
│   ├── app
│   │   └── **/*.tsx
│   ├── index.d.ts
│   ├── index.html
│   ├── main.ts
│   ├── render-app.tsx
│   ├── preload.ts
│   └── main-utils.ts
├── package.json
└── yarn.lock
```

All code files are within the src directory

Within there the code is split up into the Electron backend process and the React UI.

### Frontend

The entrypoint for the frontend is `render-app.tsx` and has the `preload.ts` injected by Electron to provide the API to the backend.

When running with `yarn start` the React app will auto-reload with changes to the directory. Unfortunately that only applies the frontend.

All files within the app folder are run from the frontend which does not have access to any `node` APIs.

::: warning
When importing within files in the `app` directory make sure the imports to not reference the `node` API as it will fail to render with the following error:
```
An unhandled rejection has occurred inside Forge:
Error: EISDIR: illegal operation on a directory, read
```
:::

### Backend

The backend entrypoint is the `main.ts` and is small wrapper which just starts up and holds a reference to the `App` class in `app.ts` where the application gets started.

Other files of note are `main-utils.ts` which re-exports `utils.ts` but adds additional utilities that reference `node` and Electron APIs



## Building Executables

For publishing all executables are built on Linux.

Once the publish command is successful the output files can be found in `out/make/<maker>/<platform>`

### Windows

Can be build natively on Windows without any other prerequisites. For Linux part of the build process requires Wine (both x86 and x64) and `mono` to be installed.

For Ubuntu the full dependency command is as follows

```
dpkg --add-architecture i386
apt-get update
apt-get install -y \
  git \
  wine32 \
  wine64 \
  mono-runtime \
  libmono-microsoft-build4.0-cil \
  libmono-microsoft-csharp4.0-cil \
  libmono-system-componentmodel-composition4.0-cil \
  libmono-system-data-services-client4.0-cil \
  libmono-system-io-compression-filesystem4.0-cil \
  libmono-system-runtime-serialization4.0-cil \
  libmono-system-servicemodel4.0a-cil \
  libmono-system-xml-linq4.0-cil \
  libmono-windowsbase4.0-cil
```

Once all dependencies are installed the application can be built with
```
yarn make-win
```

### macOS

Can be built on macOS or Linux. The only requirement is having `zip` installed and on the `PATH` which
come preinstalled with the OS. The Electron docs indicate that macOS binaries can also be built on Windows
but we have not had success when pulling the necessary Electron binaries.

To build:
```
yarn make-mac
```

This will build two binaries: one for Apple Silicon and one for Intel Macs

### Linux

Can only be built on Linux. This will priovide a .deb for Debian/Ubuntu and an RPM for Fedora/RHEL based distros.

The prerequisites are `fakeroot`, `rpm-build` and `dpkg`

Once those are installed, to build:

```
yarn make-linux
```

::: warning
In some environments when building you may get the following error
```
Error: Command failed with a non-zero return code (2):
fakeroot dpkg-deb --build /tmp/electron-installer--13617-q1VL7G97He23/periscope-1.0.0_amd64
```

This happens because `dpkg` expects the `/tmp/electron-installer...` folder to be permissions `755`

To fix before building run:
```
umask 022
```
Which will change your default directory permission to `755`
:::
